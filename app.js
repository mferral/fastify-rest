
const localize = require('ajv-i18n')
const fastify = require('fastify')

function build(opts={logger: true}) {
    const app = fastify(opts)
    const io = require('./socket.io')(app)

    app.register(require('fastify-cors'), { origin: "*" })

    // Validation schema
    app.setErrorHandler(function (error, request, reply) {
        if (error.validation) {
            localize.es(error.validation)
            reply.status(400).send({
                message: error.validation.map( e => e.message) 
            })
            return
        }
        reply.send(error)
    })

    // Declare a route
    app.get('/', function (request, reply) {        
        io.emit('hello',{ message: 'ok' })
        reply.send({
            hello: 'world'
        })
    })

    // Public 
    const path = require('path')
    app.register(require('fastify-static'), {
        root: path.join(__dirname, 'public'),
        prefix: '/public/', // optional: default '/'
    })

    app.get('/chat', (request, reply) => {
        try {
            reply.sendFile('index.html')
        }
        catch (e) { console.log(e) }
    });
    // Declare Auth
    require('./auth')(app)
    // Declare a routes
    const rutas = require('./src/module/router')(app, io)   
    rutas.map((route, index) => (
        app.route(route)
    ))
    return app
}

module.exports = build