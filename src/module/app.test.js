const server = require('@root/app')()

test("Root Get", async () => {
    const options = {
        method: "GET",        
        url: "/",
    };
    const data = await server.inject(options);   
    // console.info(data.body); 
    // console.info('Search Result:', JSON.stringify(data.body, null, "  "));
    expect(data.statusCode).toBe(200);
    
});