const Handler = require('./handlers');
const Schema = require('./schemas')
module.exports = (fastify, socket=null) => [
    {
        method: 'GET',
        url: '/user',
        schema: Schema.name_schema,
        handler: Handler.my_handler,
    },
    {
        method: 'POST',
        url: '/verify',
        config: {roles:['spy','admi']},
        preHandler: [fastify.authenticate, fastify.roles],        
        handler: Handler.verify,
    },
    {
        method: 'GET',
        url: '/message',
        config: {socket},
        handler: Handler.message,
    },
]