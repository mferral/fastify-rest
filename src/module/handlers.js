const SP = require('./sp')

exports.my_handler = async (request, reply) => {
    const result = await SP.my_sp("",request.query, reply)
    // return { hello: request.query.name }
    return result
    // reply.code(401).send({ message: 'foo bar error' })
}

exports.verify = async (request, reply) => {    
    return request.user
}

exports.message = async (request, reply) => {  
    reply.context.config.socket.emit('hello',{ message: 'ok' })  
    return {
        message: 'Hello'
    }
}
