module.exports = {
    name_schema: {
        querystring: {
            type: 'object',
            properties: {
                name: {
                    type: 'string'
                },
                lastname: {
                    type: 'string'
                }
            },
            required: ['name', 'lastname']
        }
    }
}