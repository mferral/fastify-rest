module.exports = (fastify) => {
    fastify.register(require('fastify-jwt'), {
        secret: 'VmYq3t6w9z$C&F)J@NcQfTjWnZr4u7x!A%D*G-KaPdSgUkXp2s5v8y/B?E(H+MbQeThWmYq3t6w9z$C&F)J@NcRfUjXn2r4u7x!A%D*G-KaPdSgVkYp3s6v8y/B?E(H+',
        sign: {
            expiresIn: '1d',
            algorithm: 'HS512', 
        },
    })

    fastify.decorate("authenticate", async (request, reply) => {
        try {
            await request.jwtVerify()
        } catch (err) {
            reply.send(err)
        }
    })

    fastify.decorate("roles", async (request, reply) => {
        try {
            console.log(reply.context.config.roles);
            console.log(request.user.role)
            // let allFounded = reply.context.config.roles.every( ai => request.user.role.includes(ai) );
            let isFounded = reply.context.config.roles.some( ai => request.user.role.includes(ai) );
            if (!isFounded) return reply.code(401).send({ message: 'foo bar error' })
        } catch (err) {
            reply.send(err)
        }
    })

    fastify.post('/sign', async (request, reply) => {
        const token = fastify.jwt.sign({ 
            iduser: 0,
            name: 'John Smith',
            role: ['admin', 'spy'],
        },   
        { expiresIn: '60d' }     
        )
        return {
            token,        
        }
    })
}