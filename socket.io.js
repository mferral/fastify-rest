module.exports = (fastify) => {
    const io = require('socket.io')(fastify.server,{
        cors: {
            origin: "*",
            methods: ["GET", "POST"]
        },
    })
    
    io.on('connection', async function(socket) {

        // console.log(socket.handshake.query.token);
        
        console.log('Connect');
        console.log(socket.handshake)
        io.emit('message', {
            id: socket.id,
            message: 'Conectado 🙋‍♂️'
        }) 
        
        // const sockets = await io.fetchSockets();
        // for (const skt of sockets) {
        //     console.log(skt.id);
        //     console.log(skt.handshake);
            // console.log(socket.rooms);
            // console.log(socket.data);
            // socket.emit(/* ... */);
            // socket.join(/* ... */);
            // socket.leave(/* ... */);
            // socket.disconnect(/* ... */);
        // }
        // socket.close()        
        // socket.on('message', function () { });
        socket.on("message", (arg) => {
            
            io.emit('message', {
                id: socket.id,
                message: arg
            })  
        });
        socket.on('disconnect', function() {
            console.log('Got disconnect!');
            console.log(socket.id);
            io.emit('message', {
                id: socket.id,
                message: 'Desconectado'
            }) 
        });
    })


    return io
}